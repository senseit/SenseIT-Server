# SenseIT - Master Central Server

The central functionality of the Node.js master server is the slave server managment.  

## Getting started

Instructions for run the master server.

### Particle Build Desktop IDE

The following commands start the central server:

    nmp install
    node app.js

## License

This project is licensed under the GPL License - see the [LICENSE.md](https://gitlab.com/senseit/SenseIT-Server/blob/master/LICENSE.md) file for details