

const os = require('os');
const mdns = require('mdns-js');

function getAdresses() {
  const interfaces = os.networkInterfaces();
  const addresses = [];
  for (const k in interfaces) {
    for (const k2 in interfaces[k]) {
      const address = interfaces[k][k2];
      if (address.family === 'IPv4' && !address.internal) {
        addresses.push(address.address);
      }
    }
  }


  return addresses[addresses.length - 1].toString().replace(/\./g, '-');
}

module.exports = function(port) {
  const service = mdns.createAdvertisement(mdns.tcp('http'), port, {
    name: 'SenseIT-Server',
    txt: {
      ip: getAdresses().toString(),
    },
  });

  service.start('0.0.0.0');

  process.stdin.resume();

  // stop on Ctrl-C
  process.on('SIGINT', function() {
    service.stop();
    console.log('Advertising stopped!');
    // give deregistration a little time
    setTimeout(function() {
      process.exit();
    }, 1000);
  });
};
