const fetch = require('node-fetch');

let timeout;

function createPromise(fetchFunction) {
  if (timeout !== undefined) {
    clearTimeout(timeout);
  }
  const FETCH_TIMEOUT = 4000;
  let didTimeOut = false;
  return new Promise(function(resolve, reject) {
    // eslint-disable-next-line prefer-const
    timeout = setTimeout(function() {
      didTimeOut = true;
      reject(new Error('Request timed out'));
    }, FETCH_TIMEOUT);
    fetchFunction
      .then(function(response) {
        // Clear the timeout as cleanup
        clearTimeout(timeout);
        if (!didTimeOut) {
          resolve(response);
        }
      })
      .catch(function(err) {
        // Rejection already happened with setTimeout
        if (didTimeOut) return;
        // Reject with error
        reject(err);
      });
  });
}

module.exports = {
  insertNewRule(link, senorPort, sensorType, outputSensorPort, next) {
    createPromise(
      fetch(link,
        {
          method: 'POST',
          mode: 'cors',
          cache: 'no-cache',
          credentials: 'same-origin',
          headers: { 'Content-Type': 'application/json' },
          redirect: 'follow',
          referrer: 'no-referrer',
          body: JSON.stringify({ sensorPort: senorPort, sensorType, outputSensorPort }),
        }),
    ).then(function() {
      next(false);
    }).catch(function() {
      next(true);
    });
  },
  getAllEvent(link, next) {
    createPromise(fetch(link))
      .then(function(response) {
        return response.json();
      }).then(function(jsonResp) {
        next(jsonResp, null);
      }).catch(function(err) {
        next(null, err);
      });
  },
  deleteRule(link, index, next) {
    createPromise(
      fetch(link,
        {
          method: 'POST',
          mode: 'cors',
          cache: 'no-cache',
          credentials: 'same-origin',
          headers: { 'Content-Type': 'application/json' },
          redirect: 'follow',
          referrer: 'no-referrer',
          body: JSON.stringify({ id: index }),
        }),
    ).then(function() {
      next(false);
    }).catch(function() {
      next(true);
    });
  },
  updateRule(link, id, senorPort, sensorType, outputSensorPort, next) {
    // eslint-disable-next-line no-var
    var correct;
    // eslint-disable-next-line prefer-template
    const deleteLink = link + '/deleteSensor';
    module.exports.deleteRule(deleteLink, id, function(error) {
      correct = error;
    });

    // eslint-disable-next-line prefer-template
    const insertLink = link + '/triggerSensor';
    module.exports.insertNewRule(insertLink, senorPort, sensorType, outputSensorPort, function(error) {
      correct = correct && error;
    });
    next(correct);
  },
};
