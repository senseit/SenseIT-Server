
const os = require('os');

// Define schema
const mongoose = require('mongoose');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);


function getAdresses() {
  const interfaces = os.networkInterfaces();
  const addresses = [];
  for (const k in interfaces) {
    for (const k2 in interfaces[k]) {
      const address = interfaces[k][k2];
      if (address.family === 'IPv4' && !address.internal) {
        addresses.push(address.address);
      }
    }
  }

  return addresses[addresses.length - 1].toString().replace(/\./g, '-');
}

const ServerAdressSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    default: 'SenseITServer',
  },
  ip: {
    type: String,
    default: getAdresses(),
  },
  port: {
    type: Number,
    default: 8080,
  },
});


// Compile model from schema
module.exports = mongoose.model('ServerAdressModel', ServerAdressSchema);
