

const DeviceModel = require('../models/DeviceModel.js');

exports.modelToDto = function(device) {
  return {
    deviceId: device.deviceId,
    name: device.name,
    lastConnection: device.lastConnection,
  };
};

exports.dtoToModel = function(deviceDTO) {
  const instance = new DeviceModel();
  instance.lastConnection = new Date();
  instance.name = deviceDTO.name;
  instance.deviceId = deviceDTO.deviceId;
  return instance;
};
