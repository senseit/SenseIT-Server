

const ServerAdressModel = require('../models/ServerAdressModel.js');
const eventController = require('../controllers/EventController');
const deviceRestController = require('./DeviceRestController');
const deviceController = require('./DeviceController');
const DeviceModel = require('../models/DeviceModel.js');

module.exports = {
  isOnline(req, res) {
    eventController.listAllEvents(req, res);
  },
  setName(req, res) {
    ServerAdressModel.find({ name: req.params.serverName }, function(err, result) {
      if (err) {
        res.status(500).send(err);
      } else if (result.length < 1) {
        res.sendStatus(404);
      } else {
        ServerAdressModel.updateOne({ name: req.params.serverName },
          { $set: { name: req.body.serverName } },
          function(error) {
            if (error) {
              res.status(500).send(error);
            } else {
              res.json({ status: 'Succesfully updated!' });
            }
          });
      }
    });
  },
  setPort(req, res) {
    ServerAdressModel.find({ name: req.params.serverName }, function(err, result) {
      if (err) {
        res.status(500).send(err);
      } else if (result.length < 1) {
        res.sendStatus(404);
      } else {
        ServerAdressModel.updateOne({ name: req.params.serverName }, { $set: { port: req.body.serverPort } },
          function(error) {
            if (error) {
              res.status(500).send(error);
            } else {
              res.json({ status: 'Done! The changes will be applied after a restart!' });
            }
          });
      }
    });
  },
  initServerIfIsNotExists(next) {
    ServerAdressModel.find({}, function(err, result) {
      if (err) console.log('Initailization error', err);
      else if (result.length < 1) {
        const newServer = new ServerAdressModel();
        newServer.save(function(error, server) {
          if (error) console.log('New evice save error!', err);
          else {
            console.log('Initialization succesfull!');
            process.env.PORT = server.port;
            process.env.ServerName = server.name;
            next();
          }
        });
      } else {
        process.env.PORT = result[0].port;
        process.env.ServerName = result[0].name;
        next();
      }
    });
  },
  searchRunningDevices() {
    console.log('Configuring saved devices');

    DeviceModel.find({}, function(err, result) {
      if (err) {
        console.log('Device initialization error!', err);
      } else {
        for (let i = 0; i < result.length; i++) {
          const { ip } = result[i];
          const { port } = result[i];
          const { deviceId } = result[i];
          DeviceModel.find({ deviceId: result[i].deviceId },
            // eslint-disable-next-line no-loop-func
            function(error, ruleResult) {
              if (error) {
                console.log('Device initialization error!', error);
              } else if (ruleResult.length >= 1) {
                const link = `http://${ip}:${port}/triggerSensor`;

                for (let j = 0; j < ruleResult.length; j++) {
                  // eslint-disable-next-line prefer-const
                  for (let k = 0; k < ruleResult[j].definedRules.length; k++) {
                    deviceRestController.insertNewRule(link, ruleResult[j].definedRules[k].InputSensorPort,
                      ruleResult[j].definedRules[k].InputSensorType,
                      ruleResult[j].definedRules[k].OutputSensorPort,
                      // eslint-disable-next-line no-loop-func
                      function(isError) {
                        let ruleMessage = '';
                        if (isError) {
                          ruleMessage = 'Device is unreachable or offline!';
                        } else {
                          ruleMessage = 'Device is online and configured!';
                          eventController.insertNewEvent(deviceId, '-', ruleMessage, 'Connection');
                        }
                      });
                  }
                }
              }
            });
        }
      }
    });
  },
  getEventsFromDevice(deviceId) {
    // eslint-disable-next-line no-var
    var firstError = true;
    const interval = setInterval(function() {
      deviceController.getAllEvent(deviceId,
        function() {
          if (firstError) {
            firstError = false;
          } else {
            eventController.insertNewEvent(deviceId, '-', 'A device is offline or disconnected', 'Connection');
            clearInterval(interval);
          }
        });
    }, 500);
  },
  updateEventList() {
    DeviceModel.find({}, function(err, result) {
      if (err) {
        console.log('event update error');
      } else {
        for (let i = 0; i < result.length; i++) {
          module.exports.getEventsFromDevice(result[i].deviceId);
        }
      }
    });
  },
};
