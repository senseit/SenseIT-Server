
// Define schema
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
autoIncrement.initialize(mongoose.connection);

const getNewDate = function() {
  return new Date();
};

const EventModelSchema = mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true,
  },
  sourceId: {
    type: String,
  },
  port: {
    type: String,
    required: true,
  },
  text: {
    type: String,
  },
  type: {
    type: String,
  },
  date: {
    type: Date,
    default: getNewDate(),
  },
});

EventModelSchema.plugin(autoIncrement.plugin,
  { model: 'EventModel', field: 'id' });

// Compile model from schema
module.exports = mongoose.model('EventModel', EventModelSchema);
