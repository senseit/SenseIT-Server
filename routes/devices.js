

const express = require('express');

const router = express.Router();
const deviceController = require('../controllers/DeviceController');

/* GET home page. */
router.route('/')
  .get(deviceController.listAllDevices)
  .post(deviceController.insertNewDevice);

router.route('/:deviceId')
  .delete(deviceController.deleteDevice)
  .put(deviceController.setDeviceNameById);

router.route('/rules/:deviceId')
  .get(deviceController.getAllRules)
  .put(deviceController.updateRule)
  .post(deviceController.insertNewRule)
  .delete(deviceController.deleteRule);


module.exports = router;
