

const express = require('express');

const router = express.Router();
const eventController = require('../controllers/EventController');

/* GET home page. */
router.route('/')
  .get(eventController.listAllEvents);

router.route('/:eventId')
  .delete(eventController.deleteEvent);

module.exports = router;
