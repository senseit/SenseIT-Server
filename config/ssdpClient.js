const mdns = require('mdns');
const deviceController = require('../controllers/DeviceController');
const eventController = require('../controllers/EventController');
const serverController = require('../controllers/ServerController');


// watch all http servers
const browser = mdns.createBrowser(mdns.tcp('http'));
const cString = 'Connection';

function onUpService(service) {
  if (service.txtRecord === undefined) {
    return;
  }

  if (service.txtRecord.DeviceID !== undefined) {
    // eslint-disable-next-line no-var
    var ip = service.txtRecord.IP;
    if (service.txtRecord.deviceType === 'Raspberry') {
      ip = ip.replace(/-/g, '.');
    }
    deviceController.insertNewDeviceOnSsdp(service.txtRecord.DeviceID, ip, service.txtRecord.Port,
      function(err, newdevice) {
        if (err) console.log('Error in insert!');
        else {
          let eString = '';
          if (newdevice) {
            console.log('New device inserted', service.txtRecord.DeviceID);
            eString = 'A new device has been connected';
          } else {
            console.log('Device with ', service.txtRecord.DeviceID, ' is working!');
            eString = 'A device has been started';
          }
          eventController.insertNewEvent(service.txtRecord.DeviceID, '-', eString, cString);
        }
      });
    serverController.getEventsFromDevice(service.txtRecord.DeviceID);
  }
}

browser.on('serviceUp', onUpService);

module.exports = browser;
