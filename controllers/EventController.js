const EventModel = require('../models/EventModel.js');


module.exports = {
  listAllEvents(req, res) {
    EventModel.find({}, function(err, result) {
      if (err) {
        res.status(500).send(err);
      } else {
        const events = new Array(0);
        for (let i = 0; i < result.length; i++) {
          events.push(result[i]);
        }
        res.json(events);
      }
    });
  },
  insertNewEvent(sourceId, port, text, type) {
    const localEvent = new EventModel();
    localEvent.sourceId = sourceId;
    localEvent.port = port;
    localEvent.text = text;
    localEvent.type = type;
    localEvent.date = new Date ();

    localEvent.save(function(err, event) {
      if (err) {
        console.log('event save error!', err);
      } else {
        console.log('Event saved: ', event.text);
        setTimeout(function() {
          EventModel.deleteOne({ id: event.id }, function(error) {
            if (err) {
              console.warn('Event delete error!', error);
            }
          });
        }, 100000);
      }
    });
  },
  deleteEvent(req, resp) {
    EventModel.deleteOne({ id: req.params.eventId }, function(err) {
      if (err) {
        resp.status(500).send(err);
      } else {
        resp.json({ status: 'Event deleted' });
      }
    });
  },
};
