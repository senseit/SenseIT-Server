/*eslint-disable*/
const expect = require('chai').expect
const testMongoUrl = process.env.MONGO_URL
var DeviceModel = require('../models/DeviceModel.js');
var mongodb = require('../config/db');

describe('DeviceModelTest', () => {
  beforeEach(() => mongodb.connectDB(testMongoUrl))
  afterEach(() => mongodb.dropDB())

  it('CheckDeviceName', () => {
    var deviceIstance = new DeviceModel;
    deviceIstance.deviceId = '1234-456';
    deviceIstance.name = 'Device1';

    expect(deviceIstance.hasName()).to.equal(true);
  })

  it('UpdateDate', () => {
    var date = new Date('December 17, 1995 03:24:00');
    var deviceIstance = new DeviceModel;
    deviceIstance.deviceId = '1234-45';
    deviceIstance.name = '1';
    deviceIstance.lastConnection = date;

    deviceIstance.updateLastConnection();
    expect(deviceIstance.lastConnection).to.not.equal(date);
  })
});