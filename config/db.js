
// Set up mongoose connection
const mongoose = require('mongoose');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

mongoose.Promise = global.Promise;
// eslint-disable-next-line
process.env.MONGO_URL = 'mongodb://admin:southpark2A@ds115434.mlab.com:15434/senseit'

module.exports = {
  connectDB(url) {
    mongoose.connect(url, { useNewUrlParser: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
  },
  dropDB() {
    const db = mongoose.connection;
    db.dropDatabase();
  },
};
