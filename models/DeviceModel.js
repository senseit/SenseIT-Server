
// Define schema
const mongoose = require('mongoose');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

const getNewDate = function() {
  return new Date();
};

const DeviceModelSchema = mongoose.Schema({
  deviceId: {
    type: String,
    required: true,
    unique: true,
  },
  ip: {
    type: String,
    required: true,
  },
  port: {
    type: String,
  },
  name: {
    type: String,
    default: 'Unknown Device',
  },
  lastConnection: {
    type: Date,
    default: getNewDate(),
  },
  definedRules: [{
    InputSensorName: String,
    InputSensorPort: String,
    InputSensorType: String,
    OutputSensorPort: String,
  },
  ],
});

DeviceModelSchema.methods = {
  hasName() {
    return this.model('DeviceModel').name !== 'Unknown Device';
  },
  updateLastConnection() {
    const today = new Date();
    this.lastConnection = today;
  },
};


// Compile model from schema
module.exports = mongoose.model('DeviceModel', DeviceModelSchema);
