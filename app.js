

const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');

const http = require('http');
const deviceRouter = require('./routes/devices');
const serverRouter = require('./routes/server');
const eventRouter = require('./routes/events');
const serverController = require('./controllers/ServerController');


const mongodb = require('./config/db');
const mdnsClient = require('./config/ssdpClient');
const mdnsServer = require('./config/ssdpServer');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/server', serverRouter);
app.use('/devices', deviceRouter);
app.use('/events', eventRouter);

mongodb.connectDB(process.env.MONGO_URL);

const server = http.createServer(app);

serverController.initServerIfIsNotExists(function() {
  server.listen(process.env.PORT);
  mdnsClient.start();
  mdnsServer(process.env.PORT);
  console.log(process.env.ServerName, 'is running on', process.env.PORT);
});

setTimeout(function() {
  serverController.searchRunningDevices();
}, 1000);


setTimeout(function() {
  serverController.updateEventList();
}, 1000);
