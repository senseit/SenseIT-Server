

const express = require('express');

const router = express.Router();
const serverController = require('../controllers/ServerController');


/* GET home page. */
router.route('/')
  .get(serverController.isOnline);

router.route('/:serverName')
  .put(serverController.setName);

router.route('/port/:serverName')
  .put(serverController.setPort);


module.exports = router;
