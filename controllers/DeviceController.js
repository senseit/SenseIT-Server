

const DeviceModel = require('../models/DeviceModel.js');
const DeviceAssembler = require('../assemblers/DeviceAssembler');
const DeviceRestContoller = require('../controllers/DeviceRestController');
const EventController = require('./EventController');

module.exports = {
  listAllDevices(req, res) {
    DeviceModel.find({}, function(err, result) {
      if (err) {
        res.status(500).send(err);
      } else {
        const photons = new Array(0);
        for (let i = 0; i < result.length; i++) {
          photons.push(DeviceAssembler.modelToDto(result[i]));
        }
        res.json(photons);
      }
    });
  },
  setDeviceNameById(req, response) {
    DeviceModel.find({ deviceId: req.body.deviceId }, function(err, result) {
      if (err) {
        response.status(500).send(err);
      } else if (result.length < 1) {
        response.sendStatus(404);
      } else {
        DeviceModel.updateOne({ deviceId: req.body.deviceId }, { $set: { name: req.body.name } },
          function(error) {
            if (error) {
              response.status(500).send(error);
            } else {
              response.json({ status: 'Succesfully updated!' });
            }
          });
      }
    });
  },
  insertNewDevice(req, res) {
    DeviceModel.find({ deviceId: req.body.deviceId }, function(err, result) {
      if (err) {
        res.status(500).send(err);
      } else if (result.length > 0) {
        res.status(409).send({ status: 'Device already exists!' });
      } else {
        const photon = DeviceAssembler.dtoToModel(req.body);
        photon.save(function(error) {
          if (error) {
            res.status(500).send(error);
          } else {
            res.json(DeviceAssembler.modelToDto(photon));
          }
        });
      }
    });
  },
  insertNewDeviceOnSsdp(deviceId, ip, port, callback) {
    DeviceModel.find({ deviceId }, function(err, res) {
      if (err) {
        callback(err, null);
      } else if (res.length > 0) {
        callback(null, false);
      } else {
        const photon = new DeviceModel();
        photon.deviceId = deviceId;
        photon.ip = ip;
        photon.port = port;
        photon.save(function(error) {
          if (error) {
            callback(error, null);
          } else {
            callback(error, true);
          }
        });
      }
    });
  },
  deleteDevice(req, resp) {
    DeviceModel.deleteOne({ deviceId: req.params.deviceId }, function(err) {
      if (err) {
        resp.status(500).send(err);
      } else {
        resp.json({ status: 'Device deleted' });
      }
    });
  },
  insertNewRule(req, response) {
    DeviceModel.find({ deviceId: req.params.deviceId }, function(err, result) {
      if (err) {
        response.status(500).send(err);
      } else if (result.length < 1) {
        response.sendStatus(404);
      } else {
        result[0].definedRules.push({
          InputSensorName: req.body.InputSensorName,
          InputSensorPort: req.body.InputSensorPort,
          InputSensorType: req.body.InputSensorType,
          OutputSensorPort: req.body.OutputSensorPort,
        });
        result[0].save();
        const link = `http://${result[0].ip}:${result[0].port}/triggerSensor`;
        DeviceRestContoller.insertNewRule(link, req.body.InputSensorPort, req.body.InputSensorType, req.body.OutputSensorPort, function(isError) {
          if (isError) {
            response.json({ status: 'Rule updated error!' });
          } else {
            response.json({ status: 'Succesfully updated!' });
          }
        });
      }
    });
  },
  getAllRules(req, response) {
    DeviceModel.find({ deviceId: req.params.deviceId }, function(err, result) {
      if (err) {
        response.status(500).send(err);
      } else if (result.length < 1) {
        response.sendStatus(404);
      } else {
        response.json({ rules: result[0].definedRules });
      }
    });
  },
  updateRule(req, response) {
    DeviceModel.find({ deviceId: req.params.deviceId }, function(err, result) {
      if (err) {
        response.status(500).send(err);
      } else if (result.length < 1) {
        response.sendStatus(404);
      } else if (result[0].definedRules.length <= req.body.ruleIndex) {
        response.sendStatus(404);
      } else {
        const newRule = {
          InputSensorName: req.body.InputSensorName,
          InputSensorPort: req.body.InputSensorPort,
          InputSensorType: req.body.InputSensorType,
          OutputSensorPort: req.body.OutputSensorPort,
        };
        // eslint-disable-next-line no-param-reassign
        result[0].definedRules[req.body.ruleIndex] = newRule;
        const link = `http://${result[0].ip}:${result[0].port}`;
        DeviceRestContoller.updateRule(link, req.body.ruleIndex, req.body.InputSensorPort, req.body.InputSensorType, req.body.OutputSensorPort, function(isError) {
          if (isError) {
            console.log('Rule update, device is unreachable');
          }
          response.json({ status: 'Succesfully updated!' });
        });
      }
    });
  },
  deleteRule(req, response) {
    DeviceModel.find({ deviceId: req.params.deviceId }, function(err, result) {
      if (err) {
        response.status(500).send(err);
      } else if (result.length < 1) {
        response.sendStatus(404);
      } else if (result[0].definedRules.length <= req.body.ruleIndex) {
        response.sendStatus(404);
      } else {
        result[0].definedRules.splice(req.body.ruleIndex, 1);
        result[0].save();
        const link = `http://${result[0].ip}:${result[0].port}/deleteSensor`;

        DeviceRestContoller.deleteRule(link, req.body.ruleIndex, function(error) {
          if (error) {
            console.log('Rule deletion, device is unreachable');
          }
          response.json({ status: 'Succesfully deleted!' });
        });
      }
    });
  },
  getAllEvent(deviceId, next) {
    DeviceModel.find({ deviceId }, function(err, result) {
      if (err) {
        console.log('Event query error', err);
      } else if (result.length >= 1) {
        const link = `http://${result[0].ip}:${result[0].port}/isOnline`;

        DeviceRestContoller.getAllEvent(link, function(jsonResp, error) {
          if (error) {
            next();
            return;
          }
          if (jsonResp.length >= 1) {
            for (const i in jsonResp) {
              EventController.insertNewEvent(deviceId, jsonResp[i].input, jsonResp[i].message, '');
            }
          }
        });
      }
    });
  },
};
